@component('mail::message')
# Hello {{ $name }},

Welcome to our new Website.

@component('mail::button', ['url' => ''])
Visit our Website
@endcomponent

Thanks,<br>
@endcomponent

