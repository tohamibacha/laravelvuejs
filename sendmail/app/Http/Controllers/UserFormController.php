<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserFormController extends Controller
{
    //this function is for submiting the form and sending an email
    public function submit(Request $request)
    {
        Log::info('An informational message.');

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            //'message' => 'required',
        ]);

        // Mail::to($request->email)->send(new WelcomeMail($request->name));

        return response()->json(null, 200);
    }
}